| Tábua de Delinear - TdD |
| ------------------------|
| **Licença:** AGPLv3|
| **Autor:** Ronaldo Silveira &lt;<ronaldo@silveira.dev.br>&gt;|
| **Descrição:** Cadastro de Livros e Fichas Bibliográficas Maçônicas|

## O que mudou (ChangeLog)
O ChangeLog (nome tradicional em programação) é uma lista de coisas que mudaram
entre uma versão ou outra da Aplicação.

### 0.0.5
##### Adicionado
* Sinopse na Tela de Livros.
* Extendido Users
* Criado Empréstimos de livros.

### Corrigido

### 0.0.4
##### Adicionado
* Tela de Login persnoalizada
* Personalização de 404 e 500

###### Corrigido
* Pequenos bugs

### 0.0.3
##### Adicionado
* Listagem de livros
* Edição/Inclusão/Exclusão de Livros

##### Corrigido
* Bugs de Fatos

### 0.0.2
##### Adicionado
* Listagem de Fatos
* Edição/Inclusão/Exclusão de Fatos

##### Corrigido
* Correção de pequenos bugs das versões anteriores

### 0.0.1
##### Adicionado
* Listagem das Fichas
* Edição/Inclusão/Exclusão de Fichas


### 0.0.0
A Primeira versão do TdD tem como objetivo apenas uma aplicação útil para que um
usuário avançado possa usar os dados da Interface do Django Admin.
##### Adicionado
* Criado Estrutura básica Django
* Criado .GitIgnore
* Adicionado requeriments.txt
* Definida a Licença (AGPL v3)
* Adicionado a Estrutura do BD
