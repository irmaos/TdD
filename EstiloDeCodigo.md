**Índice**

[[_TOC_]]

Para o desenvolvedor que for colaborar atenção para o Estilo de Código da
 Aplicação

São válidas para este documento todas as Regras definidas para o [PEP8](https://wiki.python.org.br/GuiaDeEstilo)

## Indentação
* Não use Tab (Tabulação) para a identação;
* Use 4 espaços para o Código Django e 2 espaços para o Código HTML

## Comprimento da linha
* coloque no máximo 80 caracteres por linha;

## Linhas em brando
* Separe Classes e funções por duas linhas em Branco
* Separe os Mètodos da Classe com uma linha em branco apenas

## Import
* Faça os imports em linhas separadas.

     import sys
     import os

* Mas pode-se previous_page_number

     from types import StringType, ListType

* Faça todos os imports no ínicio do arquivo

* Nos imports seguir a ordem:
  1. Biblioteca Padrão
  2. Módulos Grandes inter relacionados
  3. Módulos específicos

## Espaços

* Não acrescente espaços
  1. dentro de Chaves, Colchetes ou Parênteses
  2. antes de uma vírgula
  3. antes do parenteses de argumento de uma função
  4. antes da chave de Índice

* Para clareza acrescente espaço antes e depois do igual de uma atribuição.

     var = 0

* O mesmo vale para os outros operadores Binários =, ==, <, >, !=; <>; <=, >=,
  in, not in, is, and, or, not

* Não use espaços no = quando ele definir um valor padrão.
~~~python
def complex(real, imag=0.0):
     return magic(r=real, i=imag)
~~~     

## Comentários

Não podem explicitar o código. Se isso é necessário é porque o código é muito
 ruim.

## Nomes

* Todos em Português
* Para garantir clareza do Código, NUNCA  use uma única letra como nome, exceto
 nos casos já consagrados.
* Nomes de Classe usam o Padrão SemEspaçoENomeIniciandoComMaiúscula. Claro, sem
 acentos.
* Excption será chamada de Error ou error
* Nome de Função inicia com minúscula masSegueOPadrãoDeClasses.


## Tuplas
### Model
Deverá  estar em várias linhas para clareza do Código

~~~python
dia = models.DateField(
    'Data',
    blank = True,
    null = True,
)
~~~


## Templates

Deixe espaço entre as marcações do Jinja

~~~html
{% if inromacao %}
  {{ dado.id }}
{% else %}
  {# Isso é Comentário #}
  do.something
{% endif %}
~~~

coloque o nome do bloco no Endblock
~~~html
{% block content%}
  something here
{% endblock content%}
~~~

## Sugestões
* Instale o Atom-beauty e "passe" no código após terminar a edição
  * Faça as correçõs para evitar linhas muito longas.
