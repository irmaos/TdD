**Índice**

[[_TOC_]]
## Como contribuir com o Projeto

#### Desenvolvendo
Se você é programador, você pode desenvolver código para:
* Melhorar funcionalidades existentes;
* Criar novas funcionalidades;
* Corrigir __Bugs__ da Aplicação;

Para tanto siga os seguintes passos:
* Pegue uma __Issue__, caso não exista ainda, crie e assine.
* Desenvolva o Código em uma __branch__ separada da Master com nome óbvio
* Peça __merge__ para a Master

Veja detalhes no [Regras de Código](RegrasDeCodigo.md)

#### Reportando Bugs ou Pedindo funcionalidades

Caso você não seja um programador/desenvolvedor, pode colaborar apontando erros
 que você encontrou na Aplicação.

Crie uma __Issue__:
* com o__Bug__ encontrado, ou
* com a funcionalidade que acha necessário.


#### Revisando e Corrigindo a Documentação

Você também pode rever toda a Documentação do Projeto e propor modificações e/ou
 Correções.

#### Fazendo um __Commit__

Ao fazer um __Commit__ lembre-se que o nome deve ser descritivo. Assim um nome de
__commit__ sendo assim:

**É ruim** usar um nome como "correções".

**É melhor** usar algo como "Correções do Botão Salvar com ajuste no código de view"

Antes do nome do Commit inclua **obrigatoriamente** o número da __Issue__, então
  o correto seria:

**É bom** "#1234 Correções do Botão Salvar com ajuste no código de view"

Após commitar fechar a __Issue__.
