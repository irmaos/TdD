**Índice**

[[_TOC_]]

## Tábua de Delinaear (TdD |#X| ) - __Read Me 4 Dummies__
  **Licença**: AGPLv3 - Affero Gnu Public License version 3<br>
  **Autor**: Ronaldo Silveira - <ronaldo@silveira.dev.br><br>
  **Versão**: 0.0.2 - Alpha

##### A. Pré Requisitos
Estou supondo que para seguir estas instruções você está em uma máquina com
 **GNU/Linux Debian** instalado e devidamente configurado. Nesta versão do ReadMe
 para TdD |#X| não trataremos detalharemos outras distros ou SOs. Mas os comandos
 devem servir para Distros derivadas como o __Ubuntu__ e o __Mint__.<br>
As instruções daqui podem parecer massantes para um __dev__ experiente, e
 provavelmente são inúteis. Mas elas visam atender o irmão com o mínimo (quase
 nenhuma) de experiência com desenvolvimento. que queria __subir__ um cópia da
 Aplicação em seu servidor ou sua máquina pessoal.<br>
 Vamos instruir o óbvio, pois, para o iniciante (Aprendiz de __Dev__) nada é óbvio.


### 1. Visão Geral do TdD

##### A. O Nome
A Aplicação se chama Tábua de Delinear, chonhecida também por alguns com __Prancha
 da Loja__, __Prancha do Mestre__ ou ainda __Tábua do Mestre__.

Pelo fato do nome ser longo, quase sempre nos referimos a Aplicação com TdD.

##### B. Propósito

O Módulo TdD |#X| é um módulo programa (CIM)para fazer registros de dados para
 pesquisa e estudo de irmãos.

Ele tem por propósito:
* Cadastrar livros físicos e Digitais
* Permitir Download de Livros Digitais
* Cadastrar Fichas de estudo
* Cadastrar Fatos Históricos importantes, formando uma linha do Tempo
* Permitir consulta em dados cadastrados.

**Advertência**
 Não haverá, em nenhuma hipótese, esclarecimentos sobre as Regras de Negócio.

### 2. Ficha Técnica
| Chave | Valor |
|------:|:------|
| **Licença** | Affero Gnu Public License (AGpl) versão 3 ou Superior |
| **Linguagem** | Python3 e __Framework__ Django3 |
| **Front** |HTML5, CSS3 com __Framework__ Bootstrap4 |
| **BD** | MariaDB ou Postgree|

O TdD é fornecido SEM NENHUMA GARANTIA de que vá servir a qualquer propósito e
ainda SEM GARANTIA de que cause danos em sua Máquina ou  em quaisquer de seus
dados.

### Veja também
* [Como Contribuir com o Projeto](CONTRIBUTING.md)
* [Como Instalar em sua Máquina](Install.md)
* [Leia a Licença Completa da Aplicação](LICENSE)
