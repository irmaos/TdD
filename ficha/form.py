from django import forms
from django.forms import ModelForm
from .models import (Acontecimento, Autor, Cidade, Editora, Ficha, Publicacao,
                    Profile, Emprestimo)


class FichaFrm(forms.ModelForm):
    class Meta:
        model = Ficha
        fields = [
            'ficha',
            'grau',
            'texto',
            'publicacao',
            'pagina',
            'img_ficha',
        ]


class FatoFrm(forms.ModelForm):
    class Meta:
        model = Acontecimento
        fields = [
            'ano',
            'seculo',
            'ac',
            'dia',
            'tipo',
            'fato',
            'descricao',
            'publicacao',
            'pagina',
        ]


class LivroFrm(forms.ModelForm):
    class Meta:
        model = Publicacao
        fields = [
            'publicacao',
            'subtitulo',
            'grau',
            'autor',
            'editora',
            'cidade',
            'edicao',
            'dt_livro',
            'capa_img',
            'digital',
            'arquivo',
            'tipo',
            'categoria',
            'nota',
            'cadastrado'
        ]


class UserFrm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            'user',
            'grau',
            'loja',
            'niver'
        ]


class EmprestaFrm(forms.ModelForm):
    class Meta:
        model = Emprestimo
        fields = [
            'dt_emprestimo',
            'dt_devolucao',
            'user',
            'devolvido',
            'obs'
        ]
