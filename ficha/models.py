from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from datetime import datetime

class Autor (models.Model):
    sobrenome = models.CharField(
        "Sobrenome do Autor",
        max_length = 100,
        blank = False,
        help_text = 'Sobrenome do Autor'
    )

    nome = models.CharField(
        "Primeiro nome do Autor",
        max_length = 100,
        blank  = True,
        help_text = 'Primeiro nome e Sobrenomes do Meio do Autor'
    )

    class Meta:
        ordering = ["sobrenome", "nome"]
        verbose_name = "Autor"
        verbose_name_plural = "Autores"

    def __str__(self):
        return '%s, %s' % (self.sobrenome, self.nome)


class Editora (models.Model):
    editora = models.CharField(
        "Editora",
        max_length = 70,
        help_text = 'Nome da Editora sem usar o prefixo Editora. Ex. Vozes em vez de Editora Vozes',
    )

    class Meta:
        ordering = ["editora"]
        verbose_name = "Editora"
        verbose_name_plural = "Editoras"

    def __str__(self):
        return '%s' % (self.editora)


class Cidade (models.Model):
    cidade = models.CharField(
        "Local da Publicação",
        max_length = 50,
        help_text = 'Nome Completo da Cidade de Publicação sem abreviações'
    )

    class Meta:
        ordering = ["cidade"]
        verbose_name = "Cidade"
        verbose_name_plural = "Cidades"

    def __str__(self):
        return '%s' % (self.cidade)


class Publicacao (models.Model):
    TIPO_CHOICES = (
        (u'Livro', u'Livro'),
        (u'Artigo', u'Artigo'),
        (u'Site', u'Site'),
        (u'Revista', u'Revista'),
        (u'Jornal', u'Jornal'),
        (u'Prancha', u'Prancha'),
    )

    CAT_CHOICES = (
        (u'Hist', u'História da Maçonaria'),
        (u'Simb', u'Simbolismo e Catecismo'),
        (u'Rito', u'Rito, Ritualística e Paramaçônicas'),
        (u'Leg', u'Legislação Maçônica'),
        (u'Sup', u'Graus Superiores'),
        (u'Dic', u'Dicionário'),
        (u'Esot', u'Esoterismo'),
        (u'Filo', u'Filosofia'),
        (u'Div', u'Diversos'),
        (u'Outr', u'Outros'),
    )

    GRAU_CHOICES = (
        (u'AM', u'A∴M∴'),
        (u'CM', u'C∴M∴'),
        (u'MM', u'M∴M∴'),
        (u'MI', u'M∴I∴'),
        (u'Fl', u'Filos∴'),
        (u'Pf', u'Prof∴'),
    )

    publicacao = models.CharField(
        "Título",
        max_length = 150,
        blank = False,
        help_text = 'Nome da Publicacao sem abreviações, idêntico ao que está na capa.'
    )

    subtitulo = models.CharField(
        "Subtítulo",
        max_length = 150,
        blank = True,
        help_text = 'Subtítulo do Livro, quando existir.'
    )

    grau = models.CharField(
        "Grau",
        max_length = 2,
        choices=GRAU_CHOICES,
        default='AM',
        blank = False,
        help_text = 'Grau das Informações contidas no Livro'
        )

    autor = models.ForeignKey(
        "Autor",
        on_delete = models.CASCADE,
        help_text = 'Escolha na lista o(s) autor(es) da Publicação.'
    )

    editora = models.ForeignKey(
        "Editora",
        on_delete = models.CASCADE,
        help_text = 'Escolha na Lista a Editora que publicou.'
    )

    cidade = models.ForeignKey(
        "Cidade",
        on_delete = models.CASCADE,
        help_text = 'Escolha a Cidade na qual ocorreu esta publicacao'
    )

    edicao = models.CharField(
        "Edição/Número",
        max_length = 25,
        null = True,
        blank = True,
        default = 'ª Ed',
        help_text = 'Coloque qual edição. Se não tiver edição deixe em branco'
    )

    dt_livro = models.CharField(
        "Data da Publicação",
        max_length = 10,
        blank = False,
        default = 's/d',
        help_text = 'Coloque a data de Publicação. Se não tiver, mantenha o default que é s/d (Sem data)'
    )

    capa_img = models.ImageField(
        upload_to='static/img_bd',
        blank = True,
        help_text = 'Escolha o arquivo com a imagem de capa. Documentos digitais sem capa receberão uma capa default'
    )

    digital = models.BooleanField(
        "Versão Digital?",
        default =  False,
        help_text = 'Mantenha false para publicação física e use True para publicação digital'
    )

    arquivo = models.FileField(
        'Se digital, faça o upload',
        upload_to = 'static/livros_pdf',
        blank = True,
        help_text = 'Se a publicação é digital suba o arquivo para facilitar o download.'
    )

    tipo = models.CharField(
        "Tipo de Material",
        max_length = 15,
        choices=TIPO_CHOICES,
        default='Livro',
        blank = False,
        help_text = 'Escolha na lista o tipo de publicação. Livro é o padrão'
    )

    categoria = models.CharField(
        "Categoria",
        max_length = 15,
        choices= CAT_CHOICES,
        default='Hist',
        blank = False,
        help_text = 'Escolha na lista a Categoria do assunto da Publicação'
    )

    sinopse = models.TextField(
        "Sinopse",
        blank = True,
        default = "Sinopse não cadastrada.",
        help_text = 'Faça um resumo crítico do livro'
    )

    nota = models.IntegerField(
        "Nota",
        default = 0,
        validators=[
            MaxValueValidator(10),
            MinValueValidator(0)
        ],
        help_text = 'Dê a nota para o livro (de 0 a 10)'
    )

    cadastrado = models.BooleanField (
        "Jà foi cadastrado?",
        default = False,
        help_text = 'Marque caso o livro já tenha sido lido e cadastrado'
    )
    class Meta:
        ordering = ["publicacao","autor"]
        verbose_name = "Publicação"
        verbose_name_plural = "Publicações"

    def __str__(self):
        return '%s. %s (%s), %s. %s: %s, %s' % (
        self.autor,
        self.publicacao,
        self.subtitulo,
        self.edicao,
        self.cidade,
        self.editora,
        self.dt_livro,
        )


class Acontecimento(models.Model):
    TIPO_CHOICES = (
        (u'Prof',u'Prof∴'),
        (u'Maç',u'Maç∴'),
        (u'Maç Br',u'Maç∴ Br∴'),
    )

    SECULO_CHOICES = (
        (u'01', u'I'),
        (u'02', u'II'),
        (u'03', u'III'),
        (u'04', u'IV'),
        (u'05', u'V '),
        (u'06', u'VI'),
        (u'07', u'VII'),
        (u'08', u'VIII'),
        (u'09', u'IX'),
        (u'10', u'X'),
        (u'11', u'XI'),
        (u'12', u'XII'),
        (u'13', u'XIII'),
        (u'14', u'XIV'),
        (u'15', u'XV'),
        (u'16', u'XVI'),
        (u'17', u'XVII'),
        (u'18', u'XVIII'),
        (u'19', u'XIX'),
        (u'20', u'XX'),
        (u'21', u'XXI')
    )

    ano = models.PositiveIntegerField(
        'Ano',
        blank = True,
        default = 2020,
        validators=[
            MaxValueValidator(2100),
            MinValueValidator(1)
        ],
        help_text = 'Escolha o Ano em que ocorreu o fato'
    )

    seculo = models.CharField(
        'Século',
        max_length = 2,
        choices = SECULO_CHOICES,
        blank = False,
        default = '21',
        help_text = 'Escolha o século no qual o fato ocorreu'
    )

    ac = models.BooleanField(
        "a.C.?",
        default =  False,
        help_text = 'Marque caso o fato registrado seja Antes de Cristo.'
    )

    dia = models.DateField(
        'Data',
        blank = True,
        null = True,
        help_text = 'Data exata do Fato, quando conhecida'
    )

    tipo = models.CharField(
        'Tipo',
        max_length = 8,
        choices = TIPO_CHOICES,
        default='Maç',
        help_text = 'Escolha na Lista o Tipo/Categoria do Fato'
    )

    fato = models.CharField(
        'Acontecimento',
        max_length = 250,
        blank = False,
        help_text = 'Descrição do Fato de forma mínima'
    )

    descricao = models.TextField(
        'Descrição',
        blank = True,
        help_text = 'Detalhamento ou Texto original sobre o fato'
    )

    publicacao = models.ForeignKey(
        'publicacao',
        on_delete = models.CASCADE,
        blank = False,
        help_text = 'Escolha na lista onde está  a informação registrada'
    )

    pagina = models.TextField(
        'Pág.',
        max_length = 5,
        blank = True,
        help_text = 'Indique a página da qual a informação foi extraída'
    )

    class Meta:
        ordering = ['seculo','ano']
        verbose_name = 'Acontecimento'
        verbose_name_plural = 'Acontecimentos'

    def __str__(self):
        return '%s (%s) - %s - %s - %s' % (
            self.ano,
            self.seculo,
            self.tipo,
            self.fato,
            self.publicacao,
            )


class Ficha(models.Model):
    GRAU_CHOICES = (
        (u'AM', u'A∴M∴'),
        (u'CM', u'C∴M∴'),
        (u'MM', u'M∴M∴'),
        (u'MI', u'M∴I∴')
    )

    ficha = models.CharField(
        "Título da Ficha",
        max_length = 200,
        blank = False,
        help_text = 'Dê um nome que facilite a pesquisa da ficha, não precisa ser único.'
    )


    grau = models.CharField(
        "Grau",
        max_length = 2,
        choices=GRAU_CHOICES,
        default='AM',
        blank = False,
        help_text = 'Escolha o Grau da Informação'
    )

    texto = models.TextField(
        'Texto da Publicação (Formato HTML)',
        unique = True,
        help_text = 'Usando HTML para facilitar a leitura coloque o trecho do Livro para posterior pesquisa'
    )

    publicacao = models.ForeignKey(
        'publicacao',
        on_delete = models.CASCADE,
        help_text = 'Escolha na lista o Livro do qual a informação da Ficha foi retirada'
    )

    pagina = models.CharField(
        "Pág.",
        max_length = 5,
        blank = 'True',
        help_text = 'A página inicial do livro de onde a informação foi retirada. Se preferir use Página Inicial e Final separada por hífem 00-00'
    )

    img_ficha = models.ImageField(
        'Imagem de Apoio (Se houver)',
        upload_to = 'static/img_ficha',
        null = True,
        blank = True,
        help_text = 'Se for colocar uma imagem importante do Texto original use este campo.'
    )

    class Meta:
        ordering = ["grau", "ficha"]
        verbose_name = "Ficha"
        verbose_name_plural = "Fichas"

    def __str__(self):
        return '%s' % (
            self.ficha,
        )


class Potencia(models.Model):
    potenciasigla = models.CharField(
        "Sigla",
        max_length = 10,
        blank = False,
        null = False,
        default = "GOB-SP",
        help_text = 'Sigla da Potência'
    )

    potencia = models.CharField(
        "Potencia/Obediência",
        max_length = 200,
        blank = False,
        null =  False,
        default = "Grande Oriente do Brasil de São Paulo",
        help_text = 'Nome completo (por extenso) da Potência/Obediência.'
    )

    class Meta:
        ordering = ["potenciasigla"]
        verbose_name = "Potência"
        verbose_name_plural = "Potências"

    def __str__(self):
        return '%s' % (
            self.potenciasigla,
        )


class Loja(models.Model):
    TITULO_CHOICES = (
        (u'A∴R∴L∴S∴', u'A∴R∴L∴S∴'),
        (u'A∴R∴B∴L∴S∴', u'A∴R∴B∴L∴S∴'),
        (u'A∴R∴G∴B∴L∴S∴', u'A∴R∴G∴B∴L∴S∴'),
        (u'A∴R∴G∴B∴L∴S∴', u'A∴R∴G∴B∴L∴S∴'),
        (u'A∴R∴G∴B∴B∴V∴L∴S∴', u'A∴R∴G∴B∴B∴V∴L∴S∴'),
        (u'A∴R∴G∴B∴B∴V∴L∴S∴', u'A∴R∴G∴B∴B∴V∴L∴S∴'),
    )

    titulo = models.CharField(
        "Título",
        max_length = 20,
        choices = TITULO_CHOICES,
        blank = False,
        null = False,
        default = 'A∴R∴L∴S∴',
        help_text = 'Escolha na lista o título abreviado da Loja (padrão)'
    )

    loja = models.CharField(
        "Loja",
        max_length = 100,
        blank = False,
        null = False,
        default = ' ',
        help_text = 'Nome completo da Loja, sem abrevisções, sem número e sem título'
    )

    numero = models.PositiveIntegerField(
        "Nº",
        blank = False,
        default = 1,
        validators=[
            MaxValueValidator(99999),
            MinValueValidator(1)
        ],
        help_text = 'Número da Loja'
    )

    potencia = models.ForeignKey(
        "Potencia",
        on_delete = models.CASCADE,
        help_text = "Escolha uma Potência da Lista"
    )

    class Meta:
        ordering = ["loja"]
        verbose_name = "Loja"
        verbose_name_plural = "Lojas"

    def __str__(self):
        return '%s %s Nº %s (%s)' % (
            self.titulo,
            self.loja,
            self.numero,
            self.potencia,
        )


class Profile(models.Model):
    GRAU_CHOICES = (
        (u'AM', u'A∴M∴'),
        (u'CM', u'C∴M∴'),
        (u'MM', u'M∴M∴'),
        (u'MI', u'M∴I∴')
    )

    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE,
        help_text = 'Escolha um usuário/cim para completar os dados.'
    )

    grau = models.CharField(
        "Grau",
        max_length = 2,
        choices = GRAU_CHOICES,
        default = 'AM',
        blank = False,
        help_text = 'Grau Simbólico do Irmão',
    )

    loja = models.ForeignKey(
        "Loja",
        on_delete = models.CASCADE,
        help_text = 'Loja do Irmão, no caso de duas filiações escolher a principal'
    )

    niver = models.DateField(
        "Aniversário",
        null=True,
        blank=True,
        help_text = 'Data de Nascimento do Irmão',
    )

    class Meta:
        ordering = ["user"]
        verbose_name = "Irmão"
        verbose_name_plural = "Irmãos"

    def __str__(self):
        return '%s (%s) %s' % (
            self.user,
            self.grau,
            self.loja,
        )


class Emprestimo(models.Model):
    dt_emprestimo = models.DateField(
        "Data do empréstimo",
        null = False,
        blank = False,
        default = datetime.now,
        help_text = 'Data na qual o irmão pegou o livro'
    )

    dt_devolucao = models.DateField(
        "Data da Devolução",
        null = False,
        blank = False,
        default = datetime.now,
        help_text = 'Data prevista para devolução'
    )

    user = models.ForeignKey(
        Profile,
        on_delete = models.CASCADE,
        help_text = 'Escolha na Lista o Irmão que pegou o livro emprestado'
    )

    publicacao = models.ForeignKey(
        "publicacao",
        on_delete = models.CASCADE,
        help_text = 'Escolha na lista o livro emprestado'
    )

    devolvido = models.BooleanField(
        "Livro Devolvido",
        default = False,
        help_text = 'Após a devolução marque para dar baixa'
    )

    obs = models.TextField(
        "Observação",
        default = " ",
        blank = True,
        null = True,
        help_text = 'Anote observações que achar importante sobre o livro, danos, perdas, atrasos, estravios ou quaiquer coisas que achar '
    )
    class Meta:
        ordering = ["devolvido", "dt_emprestimo","dt_devolucao"]
        verbose_name = "Empréstimo"
        verbose_name_plural = "Empréstimos"

    def __str__(self):
        return '%s → %s (%s) %s' % (
            self.dt_enprestimo,
            self.dt_devolucao,
            self.user,
            self.publicacao,
        )
