from django.contrib import admin
from .models import (Acontecimento, Autor, Cidade, Editora, Ficha, Publicacao,
                    Potencia, Loja, Profile)
                    # , Emprestimo)

class PotenciaAdmin(admin.ModelAdmin):
    pass


class LojaAdmin(admin.ModelAdmin):
    pass


class ProfileAdmin(admin.ModelAdmin):
    pass


class AutorAdmin(admin.ModelAdmin):
    pass


class EditoraAdmin(admin.ModelAdmin):
    pass


class CidadeAdmin(admin.ModelAdmin):
    pass


class EmprestimoAdmin(admin.ModelAdmin):
    pass


class PublicacaoAdmin(admin.ModelAdmin):
    list_display = (
        'publicacao',
        'subtitulo',
        'autor',
        'tipo',
        'categoria',
    )


class FichaAdmin(admin.ModelAdmin):
    list_display = (
        'ficha',
        'grau',
        'pagina',
        'publicacao',
    )

    list_filter = (
        'grau',
    )

    search_fileds = [
        'ficha',
        'texto',
    ]

    list_per_page = 50


class AcontecimentoAdmin(admin.ModelAdmin):
    list_display = (
        'ano',
        'seculo',
        'dia',
        'tipo',
        'fato',
    )

    list_filter = (
        'tipo',
    )

    search_fileds = [
        'fato',
        'descricao',
    ]

    list_per_page = 50


admin.site.register(Potencia, PotenciaAdmin),
admin.site.register(Loja, LojaAdmin),
admin.site.register(Profile, ProfileAdmin),
admin.site.register(Autor, AutorAdmin),
admin.site.register(Editora, EditoraAdmin),
admin.site.register(Cidade, CidadeAdmin),
admin.site.register(Publicacao, PublicacaoAdmin),
admin.site.register(Acontecimento, AcontecimentoAdmin),
admin.site.register(Ficha, FichaAdmin),
# admin.site.register(Emprestimo, EmprestimoAdmin),
