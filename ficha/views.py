from django.shortcuts import redirect, render
from django.core.paginator import Paginator
from django.db.models import Q
from django.contrib.auth.decorators import login_required

from .form import FichaFrm, FatoFrm, LivroFrm, UserFrm, EmprestaFrm
from .models import Acontecimento, Ficha, Publicacao, Profile, Emprestimo

""" Páginas Estáticas ------------------------------------------------------ """
def home(request):
    return render(request, 'index.html')


def sobre(request):
    return render(request, 'sobre.html')


def gpl(request):
    return render(request, 'licenca.html')


""" Fichas (Bibliográficas Maçônicas) ------------------------------------- """

@login_required
def ficha(request):
    fichas = Ficha.objects.all().order_by('-id')
    mysearch = request.GET.get('q')
    if mysearch:
        fichas = Ficha.objects.filter(
            Q(ficha__icontains=mysearch)|
            Q(texto__icontains=mysearch)
        )

    paginator = Paginator(fichas, 5)
    page = request.GET.get('page')
    fichas = paginator.get_page(page)
    return render(request, 'ficha.html',{'fichas':fichas})


@login_required
def fichaNova(request):
    form = FichaFrm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.save()
        return redirect('url_ficha')
    else:
        form = FichaFrm()

    return render(request, "ficha_nova.html",{'form':form})


@login_required
def fichaEdita(request, pk):
    data = {}
    fichas = Ficha.objects.get(pk=pk)
    form = FichaFrm(request.POST or None, instance=fichas)

    if form.is_valid():
        form.save()
        return redirect('url_ficha')

    data['form']=form
    data['ojb']= ficha
    return render(request, 'ficha_nova.html', {'form':form})


@login_required
def fichaApaga(request, pk):
    data = {}
    fichas = Ficha.objects.get(pk=pk)
    fichas.delete()

    return redirect('url_ficha')


@login_required
def fichaPdf(request):
    fichas = Ficha.objects.all().order_by('ficha')[:10]
    return render(request, 'ficha_pdf.html',{'fichas':fichas})

""" Fatos (Efemérides Maçônicas) ----------------------------------------------- """

@login_required
def fato(request):
    fatos = Acontecimento.objects.all().order_by('ano')
    mysearch = request.GET.get('q')
    if mysearch:
        fatos = Acontecimento.objects.filter(
            Q(fato__icontains=mysearch)|
            Q(descricao__icontains=mysearch)
        )

    paginator = Paginator(fatos, 10)
    page = request.GET.get('page')
    fatos = paginator.get_page(page)
    return render(request, 'fato.html',{'fatos':fatos})


@login_required
def fatoNovo(request):
    form = FatoFrm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.save()
        return redirect('url_fato')
    else:
        form = FatoFrm()

    return render(request, "fato_novo.html",{'form':form})


@login_required
def fatoEdita(request, pk):
    data = {}
    fatos = Acontecimento.objects.get(pk=pk)
    form = FatoFrm(request.POST or None, instance=fatos)

    if form.is_valid():
        form.save()
        return redirect('url_fato')

    data['form']=form
    data['ojb']= fato
    return render(request, 'fato_novo.html', {'form':form})


@login_required
def fatoApaga(request, pk):
    data = {}
    fatos = Acontecimento.objects.get(pk=pk)
    fatos.delete()

    return redirect('url_fato')


@login_required
def fatoPdf(request):
    fatos = Acontecimento.objects.all().order_by('ano')
    return render(request, 'fato_pdf.html',{'fatos':fatos})


""" livros (Cadastro e Biblioteca) --------------------------------------------- """

@login_required
def livro(request):
    livros = Publicacao.objects.all().order_by('publicacao')
    mysearch = request.GET.get('q')
    if mysearch:
        livros = Publicacao.objects.filter(
            Q(publicacao__icontains=mysearch)
        )

    paginator = Paginator(livros, 20)
    page = request.GET.get('page')
    livros = paginator.get_page(page)
    return render(request, 'livro.html',{'livros':livros })


@login_required
def livroNovo(request):
    form = FatoFrm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.save()
        return redirect('url_livro')
    else:
        form = LivroFrm()

    return render(request, "livro_novo.html",{'form':form})


@login_required
def livroEdita(request, pk):
    data = {}
    livros = Publicacao.objects.get(pk=pk)
    form = LivroFrm(request.POST or None, instance=livros)

    if form.is_valid():
        form.save()
        return redirect('url_livro')

    data['form']=form
    data['ojb']= livro #para deletar
    return render(request, 'livro_novo.html', {'form':form})


@login_required
def livroApaga(request, pk):
    data = {}
    livros = Publicacao.objects.get(pk=pk)
    livros.delete()

    return redirect('url_livro')



@login_required
def livroPdf(request):
    livros = Publicacao.objects.all().order_by('publicacao')[:50]
    return render(request, 'livro_pdf.html',{'livros':livros})


""" Users --------------------------------------------------------------------"""
@login_required
def user(request):
    users = Profile.objects.all().order_by('-id')
    mysearch = request.GET.get('q')
    if mysearch:
        users = Profile.objects.filter(
            Q(user__icontains=mysearch)|
            Q(loja__icontains=mysearch)|
            Q(potencia__icontains=mysearch)
        )

    paginator = Paginator(users, 10)
    page = request.GET.get('page')
    users = paginator.get_page(page)
    return render(request, 'irmao.html',{'users':users})

@login_required
def userNovo(request):
    form = UserFrm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.save()
        return redirect('url_irmao')
    else:
        form = UserFrm()

    return render(request, "irmao_novo.html",{'form':form})

@login_required
def userApaga(request, pk):
    data = {}
    users = Profile.objects.get(pk=pk)
    users.delete()

    return redirect('url_irmao')

@login_required
def userEdita(request, pk):
    data = {}
    users = Profile.objects.get(pk=pk)
    form = UserFrm(request.POST or None, instance=users)

    if form.is_valid():
        form.save()
        return redirect('url_irmao')

    data['form']=form
    data['ojb']= livro #para deletar
    return render(request, 'irmao_novo.html', {'form':form})


""" Empréstimos na Biblioteca ---------------------------------------------- """
@login_required
def emprestimo(request):
    emprestimos = Emprestimo.objects.all().order_by('-id')
    mysearch = request.GET.get('q')
    if mysearch:
        users = Emprestimo.objects.filter(
            Q(user__icontains=mysearch)|
            Q(loja__icontains=mysearch)|
            Q(dt_devolucao__icontains=mysearch)
        )

    paginator = Paginator(emprestimos, 10)
    page = request.GET.get('page')
    emprestimo = paginator.get_page(page)
    return render(request, 'emprestimo.html',{'emprestimos':emprestimos})

@login_required
def emprestimoNovo(request):
    form = UserFrm(request.POST or None)
    if form.is_valid():
        obj = form.save()
        obj.save()
        return redirect('url_emprestimo')
    else:
        form = EmprestaFrm()

    return render(request, "emprestimo_novo.html",{'form':form})

@login_required
def emprestimoEdita(request, pk):
    data = {}
    emprestimos = Emprestimo.objects.get(pk=pk)
    form = UserFrm(request.POST or None, instance=emprestimos)

    if form.is_valid():
        form.save()
        return redirect('url_emprestimo')

    data['form']=form
    data['ojb']= livro #para deletar
    return render(request, 'emprestimo_novo.html', {'form':form})
