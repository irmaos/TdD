# Instalar o TDD 4 __Dummies__

[[_TOC_]]

Passo a Passo para instalar o TdD

As instruções a seguir funcionam perfeitamente em uma instalação Debian10. Provavelmente, funcionará também em outras Distro GNU/Linux.

Não haverá instruções para o Windows.

### Requisitos
* Na sua Máquina já **devem** estar instalados:
 * python3
 * python3-dev
 * python3-venv python-virtualenv
 * git
* **Recomenda-se** a instalação:
 * Atom (para lidar com o código)
 * sqlitebrowser (para o BD em Dev)

Caso você não saiba o que cada um faça, procure a documentação na internet.

### Dev (desenvolvimento)

##### Crie o Diretório de trabalho

     $ mkdir TdD

##### Crie e ative sua virtualenv

     $ python3 -m venv .
     $ source bin/activate

##### Atualize o Pip e instale o Django
     (TdD) $ pip install -U pip
     (TdD) $ pip install django

##### Crie o Projeto
     (TdD) $ django-admin startproject Fichas .

Após criar o projeto certifique-se que está tudo bem rodando o servidor de desenvolvimento...

       (TdD) $ python3 manage.py runserver

... e verificando a página inicial do Django (o foguetinho) no seu navegador no endereço   http://127.0.0.1:8000/

##### Crie a App
     (TdD) $ python3 manage.py startapp ficha

##### Git

Inicie o Git     

     (TdD) $ git init

Configure com seus Dados

     (TdD) $ git config --global user.name "Seu nome entre Aspas"
     (TdD) $ git config --global user.email "seu.email.cadastrado@gitlab"    

Clone o Repositório para sua Máquina

### e Boa Sorte     

### Prod (Produção)

Ainda não concluída a Documentação
